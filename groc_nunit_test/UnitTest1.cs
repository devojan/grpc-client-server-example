using NUnit.Framework;
using System;
using System.ServiceProcess;

namespace groc_nunit_test
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        [TestCase("test_grpc_worker")]
        public void TestCoreWorkerServiceRunning(string serviceName)
        {
            ServiceController serviceController = new ServiceController(serviceName);
            Assert.AreEqual(serviceController.Status, ServiceControllerStatus.Running);
        }
        [TestCase("test_grpc_worker")]
        public void TestCoreWorkerServiceException(string serviceName)
        {
            ServiceController serviceController = new ServiceController(serviceName);
            Assert.Catch<SystemException>(() => { serviceController.Start(); });
           // Assert.Throws<SystemException>(() => { serviceController.Start(); });

        }
    }
}