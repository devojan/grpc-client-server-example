﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using grpcservice=test_grpc_worker.Protos;
using Grpc.Net.Client;
using WellKnownTypes=Google.Protobuf.WellKnownTypes;
namespace groc_nunit_test
{
    [TestFixture]
    public partial class TestGRPCService
    {
        [TestCase("http://127.0.0.1:5000")]
        public void SuccessGrpcServiceCall(string address)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            using (var channel=GrpcChannel.ForAddress("http://127.0.0.1:5000"))
            {
                var client = new grpcservice::test_service.test_serviceClient(channel);
                var response = client.manage_service(new WellKnownTypes::Empty());
                Assert.AreEqual(true, response.IsSuccess);
            }
        }
    }
}
