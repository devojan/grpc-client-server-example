﻿using System;
using grpcservice = test_grpc_worker.Protos;
using WellKnownTypes=Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using static Grpc.Net.Client.GrpcChannel;
namespace grpc_client_app
{
    class Program
    {
        static void Main(string[] args)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            using (var client_channel=GrpcChannel.ForAddress("http://192.168.0.128:5000"))
            {
                
                var client = new grpcservice::test_service.test_serviceClient(client_channel);
                var response = client.manage_service(new WellKnownTypes.Empty());
                if (response!=null)
                {
                    string val = response.IsSuccess ? "Yes" : "NO";
                    Console.Write($"had success start {val}");
                }
                else
                {
                    throw new Exception();
                }
            }
            Console.ReadLine();
        }
    }
}
