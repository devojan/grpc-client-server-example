﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace test_grpc_worker
{
    public partial class Worker : BackgroundService
    {
        #region MyRegion
        private static int _id = default;
        private static string _message = string.Empty;
        private static bool _is_success = default;
        #endregion
        #region prop
        public static int id => _id;
        public static string message => _message;
        public static bool is_success => _is_success;
        #endregion
        #region func
        #region worker_override_func
        #region ExecuteAsync
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
          
            try
            {
                if (stoppingToken.IsCancellationRequested)
                {
                    _id = 2;
                    _message = "service has been turned off";
                    _is_success = false;
                }
                else
                {
                    _id = 1;
                    _message = "working";
                    _is_success = true;
                }
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {

                _id = 3;
                _message = "service has been turned off due to exception";
                _is_success = false;
                return Task.FromException(ex);
            }
        }
        #endregion

        #region StartAsync
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            return base.StartAsync(cancellationToken);
        }
        #endregion

        #region StopAsync
        public override Task StopAsync(CancellationToken cancellationToken)
        {
            return base.StopAsync(cancellationToken);
        } 
        #endregion
        #endregion
        #endregion
    }
}
