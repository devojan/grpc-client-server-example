﻿using WellKnownTypes=Google.Protobuf.WellKnownTypes;
using GrpcCore=Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using test_grpc_worker.Protos;

namespace test_grpc_worker.Services
{
    public class TestGrpcService: test_service.test_serviceBase
    {
        #region prop
        public Worker workerProperties { get; set; }
        #endregion
        #region func
        #region grpc_override_func
        public override Task<ManageResponse> manage_service(WellKnownTypes::Empty request, GrpcCore::ServerCallContext context)
        {
            return Task.FromResult(new ManageResponse() { Id = Worker.id, IsSuccess = Worker.is_success, Message = Worker.message });
        }
        #endregion 
        #endregion
    }
}
